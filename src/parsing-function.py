import json
import string

def lambda_handler(event, context):
    text = "Hello, GL! This function processes a given text by removing punctuation, splitting it into words, and converting each word to lowercase."
    
    text = text.translate(str.maketrans('', '', string.punctuation))
    # Split text into words
    words = text.split()    
    # Convert words to lowercase
    words = [word.lower() for word in words]    
    
    return {
        "statusCode": 200,
        "headers": {
            "Content-Type": "application/json"
        },
        "body": json.dumps(words)
    }

if __name__ == '__main__':
    print(lambda_handler(None,None))