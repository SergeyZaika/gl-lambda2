#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from 'aws-cdk-lib';
import { GlLambdaStack } from '../lib/gl-lambda-stack';

const app = new cdk.App();
new GlLambdaStack(app, 'LambdaStackTest', {});