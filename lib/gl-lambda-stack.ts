import * as cdk from 'aws-cdk-lib';
import * as lambda from 'aws-cdk-lib/aws-lambda';
import { LambdaRestApi } from 'aws-cdk-lib/aws-apigateway';
import { Construct } from 'constructs'; // newly-added module

export class GlLambdaStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const function_name = 'lambda-parcing-function';
    const lambda_path = 'src'; 

    const handler = new lambda.Function(this, function_name, {
        functionName: function_name,
        runtime: lambda.Runtime.PYTHON_3_8,
        code: lambda.Code.fromAsset(lambda_path),
        handler: "parsing-function.lambda_handler"
    });    

    const api = new LambdaRestApi(this, 'ApiGatewayUsingTypescript', {
    handler: handler,
    proxy: false
    });

    api.root.addMethod('GET');
  }
}